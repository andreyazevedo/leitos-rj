const fetch = require('node-fetch');

const fetchJSON = (url, withHeaders = false) => fetch(url).then(response => {
  const json = response.json();

  if (withHeaders) {
    return json.then(data => Object.assign({ data }, { headers: response.headers }));
  }

  return json;
}).catch(err => ([{ error: true }]));

module.exports = {
  fetchJSON
};
