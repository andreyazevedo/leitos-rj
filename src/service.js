const { fetchJSON } = require('./http');

const urlSMS = 'https://web2.smsrio.org/hospitalar/censoPublico/server/controller/listarEnfermarias.php?filtro=todos&idGrupoTipoLeito=1';

// 0 - livre
// 1 - ocupado
// 2 - impedido
// 3-4 - cedido
// 5 - reservado

const filterLeito = status => leito => leito.leitoStatus === status;

const fmtHospitalData = hospital => {
  const livre = hospital.filter(filterLeito('0')).length;
  const ocupado = hospital.filter(filterLeito('1')).length;
  const impedido = hospital.filter(filterLeito('2')).length;
  const cedidoB = hospital.filter(filterLeito('3')).length;
  const cedido = hospital.filter(filterLeito('4')).length;
  const reservado = hospital.filter(filterLeito('5')).length;
  const covid = hospital.filter(leito => filterLeito('1')(leito) && leito.covid19 === '1').length;

  return { livre, ocupado, impedido, cedido: cedidoB + cedido, reservado, covid };
}

const fmtOverviewData = hospital => {
  const livre = hospital.reduce((acc, current) => acc + current.livre, 0);
  const ocupado = hospital.reduce((acc, current) => acc + current.ocupado, 0);
  const impedido = hospital.reduce((acc, current) => acc + current.impedido, 0);
  const cedido = hospital.reduce((acc, current) => acc + current.cedido, 0);
  const reservado = hospital.reduce((acc, current) => acc + current.reservado, 0);
  const covid = hospital.reduce((acc, current) => acc + current.covid, 0);

  return { livre, ocupado, impedido, cedido, reservado, covid };
}

const fmtSMSData = data => {
  const hospitals = Object.keys(data.dados);
  const dataByHospital = hospitals.map(key => fmtHospitalData(data.dados[key]));
  const overview = fmtOverviewData(dataByHospital);

  return overview;
}

const getSMSData = () => fetchJSON(urlSMS).then(fmtSMSData);


module.exports = { getSMSData };
