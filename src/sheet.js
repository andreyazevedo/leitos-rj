const { GoogleSpreadsheet } = require('google-spreadsheet');

const updateSheet = async (row) => {
  const doc = new GoogleSpreadsheet('1dnSTKYNnJ82Jui8WU1aTeWriJnmd4lIU_AWqJW1rhPs');

  await doc.useServiceAccountAuth({
    client_email: process.env.GOOGLE_SERVICE_EMAIL,
    private_key: process.env.GOOGLE_SERVICE_KEY,
  });

  await doc.loadInfo();

  const firstSheet = doc.sheetsByIndex[0];

  await firstSheet.addRow(row);
}

module.exports = { updateSheet };
