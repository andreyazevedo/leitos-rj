const { getSMSData } = require('./service');
const { updateSheet } = require('./sheet');

const main = async () => {
  const data = await getSMSData();
  const timestamp = new Date().toLocaleString("pt-BR", { timeZone: "America/Sao_Paulo" });
  const row = [timestamp, data.livre, data.ocupado, data.impedido, data.cedido, data.reservado, data.covid];

  updateSheet(row);
}

main();
